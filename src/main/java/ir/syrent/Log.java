package ir.syrent;

import org.bukkit.ChatColor;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Log {

    private final BookFix plugin = BookFix.getPlugin(BookFix.class); // Get instance of main class, you dont need this if you are putting this in your main class
    public File logsfolder; // Create File

    public void setuplogfolder() {
        if (!plugin.getDataFolder().exists()) { // Check if plugin folder exists
            plugin.getDataFolder().mkdir(); // if not then create it
        }

        logsfolder = new File(plugin.getDataFolder(), "logs"); // Set the path of the new logs folder

        if (!logsfolder.exists()) { // Check if logs folder exists
            logsfolder.mkdirs(); // if not then create it
            plugin.getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "Created the logs folder"); // Send a message to console that the folder has been created
        }
    }

    public void logToFile(String file, String message) {
        try {
            File dataFolder = plugin.getDataFolder(); // Sets file to the plugins/<pluginname> folder
            if (!dataFolder.exists()) { // Check if logs folder exists
                dataFolder.mkdir(); // if not then create it
            }
            File saveTo = new File(plugin.getDataFolder() + "/logs/", file + ".log"); // Sets the path of the new log file
            if (!saveTo.exists()) { // Check if logs folder exists
                saveTo.createNewFile(); // if not then create it
            }
            FileWriter fw = new FileWriter(saveTo, true); // Create a FileWriter to edit the file
            PrintWriter pw = new PrintWriter(fw); // Create a PrintWriter
            pw.println(message); // This is the text/message you will be writing to the file
            pw.flush();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace(); // If theres any errors in this process it will print the error in console
        }
    }

    public void SaveError(Exception e) {
        StringWriter sw = new StringWriter(); // Create StringWriter
        e.printStackTrace(new PrintWriter(sw)); // Set the StringWriter we just made to the StackTrace
        String fullStackTrace = sw.toString(); // Create a String
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss"); // Set the Time Format
        LocalDateTime now = LocalDateTime.now(); // Get the time
        logToFile(dtf.format(now).toString(), fullStackTrace); // Now we finally get to save the file!
        plugin.getServer().getConsoleSender().sendMessage(ChatColor.RED + "Error saved to log"); // Also send a message to console saying there was an error saved
    }

    public String formatMessage(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }
}