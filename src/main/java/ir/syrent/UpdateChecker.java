package ir.syrent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class UpdateChecker {

    BookFix javaPlugin = BookFix.getPlugin(BookFix.class);
    private final String localPluginVersion;
    private String spigotPluginVersion;
    private final String consolePrefix = "&b[BookFix] ";
    private final String inGamePrefix = "&8[&bBook&cFix&8] ";

    private static final int ID = 82786;
    private static final String ErrorMessage = "&cFailed to check for updates.";
    private static final String ConsoleUpdateMessage = "&aThere's a newer version available in spigotmc.org";
    private static final String InGameUpdateMessage = "&aA new update is available at:&b https://www.spigotmc.org/resources/" + ID + "/updates";
    private static final long CheckInterval = 432_000;


    public void checkForUpdate() {
        new BukkitRunnable() {
            @Override
            public void run() {
                Bukkit.getScheduler().runTaskAsynchronously(javaPlugin, () -> {
                    if (javaPlugin.getConfig().getString("Update-Notifications") != null &&
                            javaPlugin.getConfig().getString("Update-Notifications").equalsIgnoreCase("false")) return;

                    try {
                        final HttpsURLConnection connection = (HttpsURLConnection) new URL("https://api.spigotmc.org/legacy/update.php?resource=" + ID).openConnection();
                        connection.setRequestMethod("GET");
                        spigotPluginVersion = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
                    } catch (final IOException e) {
                        Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', getConsolePrefix() + ErrorMessage));
                        cancel();
                        return;
                    }

                    if (localPluginVersion.equals(spigotPluginVersion)) return;

                    Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', getConsolePrefix() + ConsoleUpdateMessage));


                    Bukkit.getScheduler().runTask(javaPlugin, () -> Bukkit.getPluginManager().registerEvents(new Listener() {
                        @EventHandler(priority = EventPriority.MONITOR)
                        public void onJoin(final PlayerJoinEvent event) {
                            if (javaPlugin.getConfig().getString("Update-Notifications") != null &&
                                    javaPlugin.getConfig().getString("Update-Notifications").equalsIgnoreCase("false")) return;

                            final Player player = event.getPlayer();
                            if (!player.hasPermission("MineableGems.notify")) return;
                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', getInGamePrefix() + InGameUpdateMessage));
                        }
                    }, javaPlugin));

                    cancel();
                });
            }
        }.runTaskTimer(javaPlugin, 0, CheckInterval);
    }

    public UpdateChecker(final JavaPlugin javaPlugin) {
        this.localPluginVersion = javaPlugin.getDescription().getVersion();
    }

    public String getConsolePrefix() {
        return consolePrefix;
    }

    public String getInGamePrefix() {
        return inGamePrefix;
    }
}