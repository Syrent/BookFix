package ir.syrent;

import org.bukkit.ChatColor;

public final class Messages {

    public static BookFix plugin = BookFix.getPlugin(BookFix.class);

    public static String prefix() {
        return ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("messages.prefix"));
    }
}
