package ir.syrent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.BookMeta;

import java.util.Objects;

import static org.bukkit.Material.WRITABLE_BOOK;

public class GetBook implements Listener {

    BookFix plugin = BookFix.getPlugin(BookFix.class);
    Messages messages = new Messages();

    public String getLog(PlayerEditBookEvent event) {
        BookMeta bookmeta = event.getNewBookMeta();
        return Messages.prefix()
                + "§cPlayer §b" + event.getPlayer().getName() + "§c at"
                + " X: §b" + (int) event.getPlayer().getLocation().getX()
                + "§c Y: §b" + (int) event.getPlayer().getLocation().getY()
                + "§c Z: §b" + (int) event.getPlayer().getLocation().getZ()
                + "§c Pages: §b" + bookmeta.getPageCount()
                + "§c Title: §b" + bookmeta.getTitle();
    }

    @EventHandler
    public void onInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        boolean enabled = plugin.getConfig().getBoolean("forceDelete");
        PlayerInventory inventory = player.getInventory();

        if (enabled) {
            if (inventory.getItemInMainHand().getType().equals(WRITABLE_BOOK)) {
                inventory.getItemInMainHand().setAmount(0);
            }
            if (inventory.getItemInOffHand().getType().equals(WRITABLE_BOOK)) {
                inventory.getItemInOffHand().setAmount(0);
            }
        }
    }


    @EventHandler
    public void onEditBook(PlayerEditBookEvent event) {

        Player player = event.getPlayer();
        BookMeta bookmeta = event.getNewBookMeta();
        int getpage = bookmeta.getPageCount();

        int page = plugin.getConfig().getInt("maximum-page");
        boolean enabled = plugin.getConfig().getBoolean("enable");

        String cancel = Objects.requireNonNull(plugin.getConfig().getString("messages.event-cancel")).replace("%page%", String.valueOf(page));
        if (enabled) {
            if (player.hasPermission("bookfix.bypass")) {
                return;
            }
            if (bookmeta.hasPages()) {
                if (getpage > page) {
                    if (player.getInventory().getItemInMainHand().getType().equals(WRITABLE_BOOK)) {
                        player.getInventory().getItemInMainHand().setAmount(0);
                        if (plugin.getConfig().getBoolean("verbose.operator")) {
                            for (Player onlineOperators : Bukkit.getOnlinePlayers()) {
                                if (onlineOperators.isOp()) {
                                    onlineOperators.sendMessage(getLog(event));
                                }
                            }
                        }
                        if (plugin.getConfig().getBoolean("verbose.console")) {
                            plugin.getServer().getConsoleSender().sendMessage(getLog(event));
                        }
                        if (plugin.getConfig().getBoolean("verbose.config")) {
                            Log log = new Log();
                            log.logToFile(log.formatMessage(), getLog(event));
                            /*ArrayList<String> list = (ArrayList<String>) plugin.getConfig().getStringList("log");
                            list.add(getLog(event));
                            plugin.getConfig().set("log", list);
                            plugin.saveConfig();*/
                        }
                        ItemStack item;
                        try {
                            item = new ItemStack(Objects.requireNonNull(Material.matchMaterial(Objects.requireNonNull(plugin.getConfig().getString("replace")).toUpperCase())));
                            player.getInventory().setItem(player.getInventory().getHeldItemSlot(), item);
                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Messages.prefix() + cancel));
                        } catch (Exception e) {
                            item = new ItemStack(WRITABLE_BOOK);
                            player.getInventory().setItem(player.getInventory().getHeldItemSlot(), item);
                            plugin.getConfig().set("replace", "WRITABLE_BOOK");
                            plugin.saveConfig();
                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Messages.prefix() + cancel));
                        }
                    }
                }
            }
        }
    }
}
