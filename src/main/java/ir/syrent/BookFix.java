package ir.syrent;

import org.bukkit.plugin.java.JavaPlugin;


public final class BookFix extends JavaPlugin {

    String maximum  = getConfig().getString("maximum-page");
    String material = getConfig().getString("replace").toUpperCase();

    @Override
    public void onEnable() {
        saveDefaultConfig();

        UpdateChecker updateChecker = new UpdateChecker(this);
        getServer().getConsoleSender().sendMessage("[BookFix] Enabling plugin...");
        if (getConfig().getBoolean("enable")) {
            getServer().getConsoleSender().sendMessage("[BookFix] Maximum page number: " + maximum);
            getServer().getConsoleSender().sendMessage("[BookFix] Replace book to: " + material);
            updateChecker.checkForUpdate();
        } else {
            getServer().getConsoleSender().sendMessage("[BookFix] §4Plugin disabled from config file!");
        }

        Log log = new Log();
        log.setuplogfolder();

        getCommand("bookfix").setTabCompleter(new TabComplete());
        getServer().getPluginManager().registerEvents(new GetBook(), this);

        getCommand("bookfix").setExecutor(new Command());
    }
}