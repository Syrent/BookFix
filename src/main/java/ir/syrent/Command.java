package ir.syrent;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
public class Command implements CommandExecutor, Listener {

    private final static BookFix plugin = BookFix.getPlugin(BookFix.class);
    String prefix = plugin.getConfig().getString("messages.prefix");

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        if (!sender.hasPermission("bookfix.admin")) {
            String permission = plugin.getConfig().getString("messages.need-permission");
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + permission));
            return true;
        }
        if (label.equalsIgnoreCase("bookfix")) {
            if (args.length == 0) {
                if (label.equalsIgnoreCase("bookfix")) {
                    sender.sendMessage("§cBookfix §9v" + plugin.getDescription().getVersion());
                    sender.sendMessage("§eCommands:");
                    sender.sendMessage("§7- §a/bookfix §breload");
                    sender.sendMessage("§7- §a/bookfix §bset_maximum <number>");
                    sender.sendMessage("§7- §a/bookfix §bverbose");
                    return true;
                }
                return true;
            }
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("reload")) {
                    plugin.reloadConfig();
                    String reload = plugin.getConfig().getString("messages.reload");
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + reload));
                    return true;
                }
                if (args[0].equalsIgnoreCase("set_maximum")) {
                    String insert = plugin.getConfig().getString("messages.insert-maximum-page");
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + insert));
                    return true;
                }
                boolean verbose = plugin.getConfig().getBoolean("verbose.operator");
                if (args[0].equalsIgnoreCase("verbose")) {
                    if (verbose) {
                        plugin.getConfig().set("verbose.operator", false);
                        sender.sendMessage("§7Verbose §cdisabled§7!");
                    } else {
                        plugin.getConfig().set("verbose.operator", true);
                        sender.sendMessage("§7Verbose §aenabled§7!");
                    }
                    plugin.saveConfig();
                    return true;
                }
                return true;
            }
            if (args.length == 2) {
                if (args[0].equalsIgnoreCase("set_maximum"))
                    if (!args[1].isEmpty()) {
                        int number = Integer.parseInt(args[1]);
                        String maximum_page = plugin.getConfig().getString("messages.maximum-page");
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + maximum_page));
                        plugin.getConfig().set("maximum-page", number);
                        plugin.saveDefaultConfig();
                        return true;
                }
                return true;
            }
            return true;
        }
        return false;
    }
}
